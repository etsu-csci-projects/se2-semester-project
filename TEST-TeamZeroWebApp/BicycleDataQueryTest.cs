﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using TeamZeroWebApp;
using TeamZeroWebApp.Controllers;
using TeamZeroWebApp.DataQuery;

namespace TEST_TeamZeroWebApp
{
	public class BicycleDataQueryTest
	{
        public AppDb Db { get; set; }
        public BicycleDataQuery DataQ { get; set; }

        [SetUp]
		public void Setup()
		{
			Db = new AppDb("server=restorefromdarien.cgeikwf35ere.us-east-1.rds.amazonaws.com;user id=admin;password=Password1!;port=3306;database=se2dbname;Convert Zero DateTime = True");
			DataQ = new BicycleDataQuery(Db);
		}

		[Test]
		public async Task Test_FindOneAsync_ReturnsOne()
        {
			await Db.Connection.OpenAsync();
            var query = new BicycleDataQuery(Db);
			BicycleData result = await query.FindOneAsync(1);
			await Db.Connection.CloseAsync();

			Assert.IsNotNull(result);
		}

		[Test]
		public async Task Test_LatestPostsAsync_ReturnsTen()
		{
			await Db.Connection.OpenAsync();
			var query = new BicycleDataQuery(Db);
			List<BicycleData> result = await query.LatestPostsAsync();
			await Db.Connection.CloseAsync();

			Assert.AreEqual(result.Count, 10);
		}

		[Test]
		public async Task Test_FindPageAsync_ReturnsSpecified()
		{
			await Db.Connection.OpenAsync();
			var query = new BicycleDataQuery(Db);
			List<BicycleData> result = await query.FindPageAsync(1, 15);
			await Db.Connection.CloseAsync();

			Assert.AreEqual(result.Count, 15);
		}

		// Not writing a test for DeleteAllAsync() atm because I don't want to nuke our Bicycle table

	}
}
