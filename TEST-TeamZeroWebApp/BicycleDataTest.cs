﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using MySqlConnector;
using System.Data;
using NUnit.Framework;
using TeamZeroWebApp;
using TeamZeroWebApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using TeamZeroWebApp.DataQuery;

namespace TEST_TeamZeroWebApp
{
    class BicycleDataTest
    {
        public AppDb Db { get; set; }
        public BicycleData Dat { get; set; }
        public BicycleController Con { get; set; }
        public BicycleDataQuery DatQ { get; set; }
        public List<int> SerialNumbers { get; set; }

        [SetUp]
        public void Setup()
        {
            Db = new AppDb("server=restorefromdarien.cgeikwf35ere.us-east-1.rds.amazonaws.com;user id=admin;password=Password1!;port=3306;database=se2dbname;Convert Zero DateTime = True");
            Dat = new BicycleData(Db);
            Con = new BicycleController(Db);
            DatQ = new BicycleDataQuery(Db);
            SerialNumbers = new List<int>();
        }

        public void PopulateDat()
        {
            Dat.ChainStay = 0.0;
            Dat.ComponentList = 0.0M;
            Dat.Construction = "Bonded";
            Dat.CustomerID = 1;
            Dat.ModelType = "Road";
            Dat.PaintID = 1;
            Dat.FrameSize = 0;
            Dat.OrderDate = DateTime.Now;
            Dat.StartDate = DateTime.Now;
            Dat.ShipDate = DateTime.Now;
            Dat.ShipEmployee = 0;
            Dat.FrameAssembler = 0;
            Dat.Painter = 0;
            Dat.WaterBottleBrazeons = 4;
            Dat.CustomName = "Test Test";
            Dat.LetterStyleID = "Script";
            Dat.StoreID = 1;
            Dat.EmployeeID = 1;
            Dat.TopTube = 0;
            Dat.HeadTubeAngle = 0;
            Dat.SeatTubeAngle = 0;
            Dat.ListPrice = 0.0M;
            Dat.SalePrice = 0.0M;
            Dat.SalesTax = 0.0M;
            Dat.SaleState = "TN";
            Dat.ShipPrice = 0.0M;
            Dat.FramePrice = 0.0M;
        }

        [Test]
        public async Task BicycleData_InsertAsync_LastInsertedId_Exists()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            Assert.IsNotNull(Dat.SerialNumber);
        }

        [Test]
        public async Task BicycleData_InsertAsync_LastInsertedId_IsInt()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            Assert.IsInstanceOf<Int32>(Dat.SerialNumber);
        }

        [Test]
        public async Task BicycleData_InsertAsync_LastInsertedId_GreaterThanZero()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            Assert.IsTrue(Dat.SerialNumber > 0);
        }

        [Test]
        public async Task BicycleData_InsertAsync_EntryExists()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            await Db.Connection.CloseAsync();

            int index = Dat.SerialNumber;
            var result = await Con.GetOne(index);
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task BicycleData_InsertAsync_AutoIncrementation()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            SerialNumbers.Add(Dat.SerialNumber);
            int index = (SerialNumbers.Count - 1);
            await Dat.DeleteAsync();
            PopulateDat();
            await Dat.InsertAsync();
            SerialNumbers.Add(Dat.SerialNumber);
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            Assert.IsTrue(SerialNumbers[index + 1] > SerialNumbers[index]);
        }

        [Test]
        public async Task BicycleData_InsertAsync_MaintainData()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            int index = Dat.SerialNumber;
            BicycleData Dat2 = await DatQ.FindOneAsync(index);
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            Assert.IsTrue(Dat.Equals(Dat2));
        }

        [Test]
        public async Task BicycleData_DeleteAsync_EntryDoesNotExist()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            int index = Dat.SerialNumber;
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            var result = await Con.GetOne(index);
            await Db.Connection.CloseAsync();

            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task BicycleData_UpdateAsync_EntryGetsChanged()
        {
            await Db.Connection.OpenAsync();
            PopulateDat();
            await Dat.InsertAsync();
            int index = Dat.SerialNumber;
            Dat.WaterBottleBrazeons = 5;
            await Dat.UpdateAsync();
            BicycleData Dat2 = await DatQ.FindOneAsync(index);
            await Dat.DeleteAsync();
            await Db.Connection.CloseAsync();

            Assert.IsTrue(Dat.SerialNumber == Dat2.SerialNumber && Dat2.WaterBottleBrazeons == 5);
        }
    }
}
