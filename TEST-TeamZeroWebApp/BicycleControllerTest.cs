using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TeamZeroWebApp;
using TeamZeroWebApp.Controllers;

namespace TEST_TeamZeroWebApp
{
	[TestFixture]
	public class BicycleControllerTest
	{
		private const string CONNECTION_STRING = "server=restorefromdarien.cgeikwf35ere.us-east-1.rds.amazonaws.com;user id=admin;password=Password1!;port=3306;database=se2dbname;Convert Zero DateTime=True";
		private AppDb appDb;
		private BicycleController bicycleController;
		private List<BicycleData> GetLatestResultExpected;
		private List<BicycleData> GetPage11ResultExpected;
		private List<BicycleData> GetPage330ResultExpected;
		private BicycleData GetOneResultExpected;
		private BicycleData PostResultExpected;
		private BicycleData PutOneResultExpected;
		private	string getLatestExpectedJson = File.ReadAllText("../../../ExpectedResults/GetLatestBicycleController.json");
		private	string getPage11ExpectedJson = File.ReadAllText("../../../ExpectedResults/GetPage11BicycleController.json");
		private	string getPage330ExpectedJson = File.ReadAllText("../../../ExpectedResults/GetPage330BicycleController.json");
		private	string getOneExpectedJson = File.ReadAllText("../../../ExpectedResults/GetOneBicycleController.json");
		private string postExpectedJson = File.ReadAllText("../../../ExpectedResults/PostBicycleController.json");
		private string PutOneExpectedJson = File.ReadAllText("../../../ExpectedResults/PutOneBicycleController.json");

		[SetUp]
		public void Setup()
		{
			
			Console.WriteLine("Starting setup process.");
			appDb = new AppDb(CONNECTION_STRING);
			GetLatestResultExpected = JsonConvert.DeserializeObject<List<BicycleData>>(getLatestExpectedJson);
			GetPage11ResultExpected = JsonConvert.DeserializeObject<List<BicycleData>>(getPage11ExpectedJson);
			GetPage330ResultExpected = JsonConvert.DeserializeObject<List<BicycleData>>(getPage330ExpectedJson);
			GetOneResultExpected = JsonConvert.DeserializeObject<BicycleData>(getOneExpectedJson);
			PostResultExpected = JsonConvert.DeserializeObject<BicycleData>(postExpectedJson);
			PutOneResultExpected = JsonConvert.DeserializeObject<BicycleData>(PutOneExpectedJson);

			Console.WriteLine("Setup complete.");
		}

		[Test, Order(1)]
		public void ConstructorTest()
		{ 
			bicycleController = new BicycleController(appDb);
			Assert.AreEqual(appDb, bicycleController.Db);
			bicycleController.Db.Connection.Close();
		}

		[Test, Order(2)]
		public void GetLatestTest()
		{
			bicycleController = new BicycleController(appDb);
			var result = bicycleController.GetLatest();
			result.Wait();
			var status = result.Status;
			var exception = result.Exception;
			var okResult = result.Result as OkObjectResult;

			Console.WriteLine($"EXPECTED: {getLatestExpectedJson.Replace('\n', ' ')}");
			Console.WriteLine($"ACTUAL: {JsonConvert.SerializeObject(okResult.Value as List<BicycleData>)}");

			Assert.IsNull(exception);
			Assert.AreEqual(TaskStatus.RanToCompletion, status);
			Assert.IsTrue(result.IsCompletedSuccessfully);
			Assert.AreEqual(GetLatestResultExpected, (okResult.Value as List<BicycleData>));
			bicycleController.Db.Connection.Close();
		}

		[Test, Order(3)]
		public void GetPageTest()
		{
			var result = bicycleController.GetPage(1, 1);
			result.Wait();
			var status = result.Status;
			var exception = result.Exception;
			var okResult = result.Result as OkObjectResult;

			Assert.IsNull(exception);
			Assert.AreEqual(TaskStatus.RanToCompletion, status);
			Assert.IsTrue(result.IsCompletedSuccessfully);
			Assert.AreEqual(GetPage11ResultExpected, okResult.Value as List<BicycleData>);
			bicycleController.Db.Connection.Close();

			result = bicycleController.GetPage(3, 30);
			result.Wait();
			status = result.Status;
			exception = result.Exception;
			okResult = result.Result as OkObjectResult;

			Assert.IsNull(exception);
			Assert.AreEqual(TaskStatus.RanToCompletion, status);
			Assert.IsTrue(result.IsCompletedSuccessfully);
			Assert.AreEqual(GetPage330ResultExpected, okResult.Value as List<BicycleData>);
			bicycleController.Db.Connection.Close();
		}

		[Test, Order(4)]
		public void GetOneTest()
		{ 
			var result = bicycleController.GetOne(1);
			result.Wait();
			var status = result.Status;
			var exception = result.Exception;
			var okResult = result.Result as OkObjectResult;

			Assert.IsNull(exception);
			Assert.AreEqual(TaskStatus.RanToCompletion, status);
			Assert.IsTrue(result.IsCompletedSuccessfully);
			Assert.AreEqual(GetOneResultExpected, okResult.Value as BicycleData);
			bicycleController.Db.Connection.Close();
		}

		[Test, Order(5)]
		public void PostTest()
		{
			var result = bicycleController.Post(PostResultExpected);
			result.Wait();
			var status = result.Status;
			var exception = result.Exception;
			var okResult = result.Result as OkObjectResult;

			Assert.IsNull(exception);
			Assert.AreEqual(TaskStatus.RanToCompletion, status);
			Assert.IsTrue(result.IsCompletedSuccessfully);
			Assert.AreEqual(PostResultExpected, okResult.Value as BicycleData);
			bicycleController.Db.Connection.Close();
		}

		[Test, Order(6)]
		public void PutOneTest()
		{ 
            using var cmd = appDb.Connection.CreateCommand();
			cmd.CommandText = "SELECT SERIALNUMBER FROM BICYCLE ORDER BY SERIALNUMBER DESC LIMIT 1;";
			appDb.Connection.Open();
			
            var reader = cmd.ExecuteReader();
			reader.Read();
			int serialNumber = reader.GetInt32(0);
			var result = bicycleController.PutOne(serialNumber, PutOneResultExpected);
			result.Wait();
			var status = result.Status;
			var exception = result.Exception;
			var okResult = result.Result as OkObjectResult;

			Assert.IsNull(exception);
			Assert.AreEqual(TaskStatus.RanToCompletion, status);
			Assert.IsTrue(result.IsCompletedSuccessfully);
			Assert.AreEqual(PutOneResultExpected, okResult.Value as BicycleData);
			bicycleController.Db.Connection.Close();

		
		}

		[Test, Order(7)]
		public void DeleteOneTest()
		{
			//get the last serial number in the table [the serial number of the bike created in the last test]
            using var cmd = appDb.Connection.CreateCommand();
			cmd.CommandText = "SELECT SERIALNUMBER FROM BICYCLE ORDER BY SERIALNUMBER DESC LIMIT 1;";
			appDb.Connection.Open();
			
            var reader = cmd.ExecuteReader();
			reader.Read();
			int serialNumber = reader.GetInt32(0);


			var result = bicycleController.DeleteOne(serialNumber);
			result.Wait();
			var status = result.Status;
			var exception = result.Exception;

			Assert.IsNull(exception);
			Assert.AreEqual(TaskStatus.RanToCompletion, status);
			Assert.IsTrue(result.IsCompletedSuccessfully);
		}
	}
}