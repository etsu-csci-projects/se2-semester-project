﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    //TODO: Modify Primary Key

    public class PreferenceDataQuery
    {
        public AppDb Db { get; }

        public PreferenceDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<PreferenceData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PREFERENCE WHERE id = @SERIALNUMBER";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<PreferenceData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PREFERENCE ORDER BY SERIALNUMBER DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM PREFERENCE";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<PreferenceData>> ReadAllAsync(DbDataReader reader)
        {
            var prefs = new List<PreferenceData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var pref = new PreferenceData(Db)
                    {
                        ItemName = reader.GetString(0),
                        Value = reader.GetDouble(1),
                        Description = reader.GetString(2),
                        DateChanged = reader.GetDateTime(3),
                        SerialNumber = reader.GetInt32(4)
                    };
                    prefs.Add(pref);
                }
            }
            return prefs;
        }
    }
}
