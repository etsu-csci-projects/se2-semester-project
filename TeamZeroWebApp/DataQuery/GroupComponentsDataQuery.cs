﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{

    public class GroupComponentsDataQuery
    {
        public AppDb Db { get; }

        public GroupComponentsDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<GroupComponentsData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM GROUPCOMPONENTS WHERE id = @SERIALNUMBER";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<GroupComponentsData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM GROUPCOMPONENTS ORDER BY SERIALNUMBER DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM GROUPCOMPONENTS";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<GroupComponentsData>> ReadAllAsync(DbDataReader reader)
        {
            var comps = new List<GroupComponentsData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var comp = new GroupComponentsData(Db)
                    {
                        GroupID = reader.GetDecimal(0),
                        ComponentID = reader.GetInt32(1),
                        SerialNumber = reader.GetInt32(2)
                    };
                    comps.Add(comp);
                }
            }
            return comps;
        }
    }
}
