﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class SampleNameDataQuery
    {
        public AppDb Db { get; }

        public SampleNameDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<SampleNameData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM SAMPLENAME WHERE id = @ID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<SampleNameData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM SAMPLENAME ORDER BY ID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM SAMPLENAME";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<SampleNameData>> ReadAllAsync(DbDataReader reader)
        {
            var names = new List<SampleNameData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var name = new SampleNameData(Db)
                    {
                        ID = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Gender = reader.GetString(3)
                    };
                    names.Add(name);
                }
            }
            return names;
        }
    }
}
