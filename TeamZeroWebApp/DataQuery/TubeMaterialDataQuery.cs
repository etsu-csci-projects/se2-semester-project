﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class TubeMaterialDataQuery
    {
        public AppDb Db { get; }

        public TubeMaterialDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<TubeMaterialData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM TUBEMATERIAL WHERE id = @TUBEID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@TUBEID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<TubeMaterialData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM TUBEMATERIAL ORDER BY TUBEID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM TUBEMATERIAL";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<TubeMaterialData>> ReadAllAsync(DbDataReader reader)
        {
            var materials = new List<TubeMaterialData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var material = new TubeMaterialData(Db)
                    {
                        TubeID = reader.GetInt32(0),
                        Material = reader.GetString(1),
                        Description = reader.GetString(2),
                        Diameter = reader.GetDouble(3),
                        Thickness = reader.GetDouble(4),
                        Roundness = reader.GetString(5),
                        Weight = reader.GetDouble(6),
                        Stiffness = reader.GetDouble(7),
                        ListPrice = reader.GetDecimal(8),
                        Construction = reader.GetString(9)
                    };
                    materials.Add(material);
                }
            }
            return materials;
        }
    }
}
