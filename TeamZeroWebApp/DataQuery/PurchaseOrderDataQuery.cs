﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class PurchaseOrderDataQuery
    {
        public AppDb Db { get; }

        public PurchaseOrderDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<PurchaseOrderData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PURCHASEORDER WHERE id = @PURCHASEID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PURCHASEID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<PurchaseOrderData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PURCHASEORDER ORDER BY PURCHASEID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM PURCHASEORDER";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<PurchaseOrderData>> ReadAllAsync(DbDataReader reader)
        {
            var orders = new List<PurchaseOrderData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var order = new PurchaseOrderData(Db)
                    {
                        PurchaseID = reader.GetInt32(0),
                        EmployeeID = reader.GetInt32(1),
                        ManufacturerID = reader.GetDecimal(2),
                        TotalList = reader.GetDecimal(3),
                        ShippingCost = reader.GetDecimal(4),
                        Discount = reader.GetDecimal(5),
                        OrderDate = reader.GetDateTime(6),
                        ReceiveDate = reader.GetDateTime(7),
                        AmountDue = reader.GetDecimal(8)
                    };
                    orders.Add(order);
                }
            }
            return orders;
        }
    }
}
