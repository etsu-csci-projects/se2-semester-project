﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{

    public class PurchaseItemDataQuery
    {
        public AppDb Db { get; }

        public PurchaseItemDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<PurchaseItemData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PURCHASEITEM WHERE id = @SERIALNUMBEr";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<PurchaseItemData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PURCHASEITEM ORDER BY SERIALNUMBER DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM PURCHASEITEM";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<PurchaseItemData>> ReadAllAsync(DbDataReader reader)
        {
            var items = new List<PurchaseItemData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new PurchaseItemData(Db)
                    {
                        PurchaseID = reader.GetInt32(0),
                        ComponentID = reader.GetInt32(1),
                        PricePaid = reader.GetDecimal(2),
                        Quantity = reader.GetDecimal(3),
                        QuantityReceived = reader.GetDecimal(4),
                        SerialNumber = reader.GetInt32(5)
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
