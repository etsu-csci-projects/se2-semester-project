﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class CustomerDataQuery
    {
        public AppDb Db { get; }

        public CustomerDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<CustomerData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CUSTOMER WHERE id = @CUSTOMERID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CUSTOMERID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<CustomerData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CUSTOMER ORDER BY CUSTOMERID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM CUSTOMER";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<CustomerData>> ReadAllAsync(DbDataReader reader)
        {
            var customers = new List<CustomerData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var customer = new CustomerData(Db)
                    {
                        CustomerID = reader.GetInt32(0),
                        Phone = reader.GetString(1),
                        FirstName = reader.GetString(2),
                        LastName = reader.GetString(3),
                        Address = reader.GetString(4),
                        ZipCode = reader.GetString(5),
                        CityID = reader.GetDecimal(6),
                        BalanceDue = reader.GetDecimal(7)
                    };
                    customers.Add(customer);
                }
            }
            return customers;
        }
    }
}
