﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class EmployeeDataQuery
    {
        public AppDb Db { get; }

        public EmployeeDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<EmployeeData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM EMPLOYEE WHERE id = @EMPLOYEEID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@EMPLOYEEID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<EmployeeData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM EMPLOYEE ORDER BY EMPLOYEEID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM EMPLOYEE";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<EmployeeData>> ReadAllAsync(DbDataReader reader)
        {
            var employees = new List<EmployeeData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var employee = new EmployeeData(Db)
                    {
                        EmployeeID = reader.GetInt32(0),
                        TaxPayerID = reader.GetString(1),
                        LastName = reader.GetString(2),
                        FirstName = reader.GetString(3),
                        HomePhone = reader.GetString(4),
                        Address = reader.GetString(5),
                        ZipCode = reader.GetString(6),
                        CityID = reader.GetDecimal(7),
                        DateHired = reader.GetDateTime(8),
                        DateReleased = reader.GetDateTime(9),
                        CurrentManager = reader.GetDecimal(10),
                        SalaryGrade = reader.GetDecimal(11),
                        Salary = reader.GetDecimal(12),
                        Title = reader.GetString(13),
                        WorkArea = reader.GetString(14)
                    };
                    employees.Add(employee);
                }
            }
            return employees;
        }
    }
}
