﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{

    public class ModelSizeDataQuery
    {
        public AppDb Db { get; }

        public ModelSizeDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<ModelSizeData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM MODELSIZE WHERE id = @SERIALNUMBER";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<ModelSizeData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM MODELSIZE ORDER BY SERIALNUMBER DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM MODELSIZE";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<ModelSizeData>> ReadAllAsync(DbDataReader reader)
        {
            var sizes = new List<ModelSizeData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var size = new ModelSizeData(Db)
                    {
                        ModelType = reader.GetString(0),
                        MSize = reader.GetDouble(1),
                        TopTube = reader.GetDouble(2),
                        ChainStay = reader.GetDouble(3),
                        TotalLength = reader.GetDouble(4),
                        GroundClearance = reader.GetDouble(5),
                        HeadTubeAngle = reader.GetDouble(6),
                        SeatTubeAngle = reader.GetDouble(7),
                        SerialNumber = reader.GetInt32(8)
                    };
                    sizes.Add(size);
                }
            }
            return sizes;
        }
    }
}
