﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class BicycleTubeUsageDataQuery
    {
        public AppDb Db { get; }

        public BicycleTubeUsageDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<BicycleTubeUsageData> FindOneAsync(int SerialNumber)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM BICYCLETUBEUSAGE WHERE id = @SERIALNUMBER";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SERIALNUMBER",
                DbType = DbType.Int32,
                Value = SerialNumber,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<BicycleTubeUsageData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM BICYCLETUBEUSAGE ORDER BY SERIALNUMBER DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM BICYCLETUBEUSAGE";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<BicycleTubeUsageData>> ReadAllAsync(DbDataReader reader)
        {
            var btus = new List<BicycleTubeUsageData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var btu = new BicycleTubeUsageData(Db)
                    {
                        SerialNumber = reader.GetInt32(0),
                        TubeID = reader.GetInt32(1),
                        Quantity = reader.GetDouble(2),
                    };
                    btus.Add(btu);
                }
            }
            return btus;
        }
    }
}
