﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class BikePartsDataQuery 
    {
        public AppDb Db { get; }

        public BikePartsDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<BikePartsData> FindOneAsync(int SerialNumber) //Following classes will use "id"
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM BIKEPARTS WHERE SerialNumber = @SERIALNUMBER";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SERIALNUMBER",
                DbType = DbType.Int32,
                Value = SerialNumber,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<BikePartsData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM BIKEPARTS ORDER BY SERIALNUMBER DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM BIKEPARTS";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<BikePartsData>> ReadAllAsync(DbDataReader reader)
        {
            var parts = new List<BikePartsData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var part = new BikePartsData(Db)
                    {
                        SerialNumber = reader.GetInt32(0),
                        ComponentID = reader.GetInt32(1),
                        SubstituteID = reader.GetDecimal(3),
                        Location = reader.GetString(4),
                        Quantity = reader.GetDecimal(5),
                        DateInstalled = reader.GetDateTime(6),
                        EmployeeID = reader.GetInt32(7)
                    };
                    parts.Add(part);
                }
            }
            return parts;
        }
    }
}
