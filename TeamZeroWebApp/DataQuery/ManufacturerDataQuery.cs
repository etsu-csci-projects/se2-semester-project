﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class ManufacturerDataQuery
    {
        public AppDb Db { get; }

        public ManufacturerDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<ManufacturerData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM MANUFACTURER WHERE id = @MANUFACTURERID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@MANUFACTURERID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<ManufacturerData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM MANUFACTURER ORDER BY MANUFACTURERID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM MANUFACTURER";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<ManufacturerData>> ReadAllAsync(DbDataReader reader)
        {
            var mans = new List<ManufacturerData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var man = new ManufacturerData(Db)
                    {
                        ManufacturerID = reader.GetDecimal(0),
                        ManufacturerName = reader.GetString(1),
                        ContactName = reader.GetString(2),
                        Phone = reader.GetString(3),
                        Address = reader.GetString(4),
                        ZipCode = reader.GetString(5),
                        CityID = reader.GetDecimal(6),
                        BalanceDue = reader.GetDecimal(7)
                    };
                    mans.Add(man);
                }
            }
            return mans;
        }
    }
}
