﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class PaintDataQuery
    {
        public AppDb Db { get; }

        public PaintDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<PaintData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PAINT WHERE id = @PAINTID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PAINTID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<PaintData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM PAINT ORDER BY PAINTID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM PAINT";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<PaintData>> ReadAllAsync(DbDataReader reader)
        {
            var paints = new List<PaintData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var paint = new PaintData(Db)
                    {
                        PaintID = reader.GetDecimal(0),
                        ColorName = reader.GetString(1),
                        ColorStyle = reader.GetString(2),
                        ColorList = reader.GetString(3),
                        DateIntroduced = reader.GetDateTime(4),
                        DateDiscontinued = reader.GetDateTime(5)
                    };
                    paints.Add(paint);
                }
            }
            return paints;
        }
    }
}
