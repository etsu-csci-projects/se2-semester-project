﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
	public class BicycleDataQuery
	{
		public AppDb Db { get; }

		public BicycleDataQuery(AppDb db)
		{
			Db = db;
		}

		// Provides syntax for a single SELECT query
		public async Task<BicycleData> FindOneAsync(int SerialNumber)
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT * FROM BICYCLE WHERE SerialNumber = @SERIALNUMBER";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SERIALNUMBER",
				DbType = DbType.Int32,
				Value = SerialNumber,
			});
			var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}

		// Provides syntax for an indiscriminate SELECT query, returning all values in the table
		public async Task<List<BicycleData>> LatestPostsAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT * FROM BICYCLE ORDER BY SERIALNUMBER DESC LIMIT 10;";
			return await ReadAllAsync(await cmd.ExecuteReaderAsync());
		}

		public async Task<List<BicycleData>> FindPageAsync(int pageNumber, int pageSize)
		{
			using var cmd = Db.Connection.CreateCommand();
			int firstRow = ((pageNumber - 1) * pageSize);
			//int lastSerialNumber = pageNumber * pageSize;
			cmd.CommandText = @"SELECT * FROM BICYCLE ORDER BY SERIALNUMBER LIMIT @FIRSTROW, @PAGESIZE";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@PAGESIZE",
				DbType = DbType.Int32,
				Value = pageSize,
			});

			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@FIRSTROW",
				DbType = DbType.Int32,
				Value = firstRow
			});
			return await ReadAllAsync(await cmd.ExecuteReaderAsync());
		}
		public async Task<List<BicycleData>> FindPageOrderByListPriceAsync(int pageNumber, int pageSize, bool ascending)
		{
			using var cmd = Db.Connection.CreateCommand();
			int firstRow = ((pageNumber - 1) * pageSize);
			if (ascending)
				cmd.CommandText = @"SELECT * FROM BICYCLE ORDER BY LISTPRICE ASC LIMIT @FIRSTROW, @PAGESIZE";
			else
				cmd.CommandText = @"SELECT * FROM BICYCLE ORDER BY LISTPRICE DESC LIMIT @FIRSTROW, @PAGESIZE";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@PAGESIZE",
				DbType = DbType.Int32,
				Value = pageSize,
			});

			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@FIRSTROW",
				DbType = DbType.Int32,
				Value = firstRow
			});
			return await ReadAllAsync(await cmd.ExecuteReaderAsync());
		}

		public async Task<List<BicycleData>> FindPageOrderByTypeAsync(int pageNumber, int pageSize, string type, bool ascending)
		{
			using var cmd = Db.Connection.CreateCommand();
			int firstRow = ((pageNumber - 1) * pageSize);
			if(ascending)
				cmd.CommandText = @"SELECT * FROM BICYCLE WHERE MODELTYPE = @MODELTYPE ORDER BY LISTPRICE ASC LIMIT @FIRSTROW, @PAGESIZE";
			else
				cmd.CommandText = @"SELECT * FROM BICYCLE WHERE MODELTYPE = @MODELTYPE ORDER BY LISTPRICE DESC LIMIT @FIRSTROW, @PAGESIZE";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@PAGESIZE",
				DbType = DbType.Int32,
				Value = pageSize,
			});

			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@FIRSTROW",
				DbType = DbType.Int32,
				Value = firstRow
			});

			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@MODELTYPE",
				Value = type
			});
			return await ReadAllAsync(await cmd.ExecuteReaderAsync());
		}



		// Provides syntax for an indiscriminate DELETE query, clearing the table
		public async Task DeleteAllAsync()
		{
			using var txn = await Db.Connection.BeginTransactionAsync();
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"DELETE FROM BICYCLE";
			await cmd.ExecuteNonQueryAsync();
			await txn.CommitAsync();
		}

		// TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

		// Employed internally by the class to read out the returned table entries for the SELECT queries
		private async Task<List<BicycleData>> ReadAllAsync(DbDataReader reader)
		{
			var bikes = new List<BicycleData>();
			using (reader)
			{
				while (await reader.ReadAsync())
				{
					var bike = new BicycleData(Db)
					{
						SerialNumber = reader.GetInt32(0),
						CustomerID = reader.GetInt32(1),
						ModelType = reader.GetString(2),
						PaintID = reader.GetInt32(3),
						FrameSize = reader.GetDouble(4),
						OrderDate = reader.GetDateTime(5),
						StartDate = reader.GetDateTime(6),
						ShipDate = reader.GetDateTime(7),
						ShipEmployee = reader.GetDecimal(8),
						FrameAssembler = reader.GetDecimal(9),
						Painter = reader.GetDecimal(10),
						Construction = reader.GetString(11),
						WaterBottleBrazeons = reader.GetDecimal(12),
						CustomName = reader.GetString(13),
						LetterStyleID = reader.GetString(14),
						StoreID = reader.GetInt32(15),
						EmployeeID = reader.GetInt32(16),
						TopTube = reader.GetDouble(17),
						ChainStay = reader.GetDouble(18),
						HeadTubeAngle = reader.GetDouble(19),
						SeatTubeAngle = reader.GetDouble(20),
						ListPrice = reader.GetDecimal(21),
						SalePrice = reader.GetDecimal(22),
						SalesTax = reader.GetDecimal(23),
						SaleState = reader.GetString(24),
						ShipPrice = reader.GetDecimal(25),
						FramePrice = reader.GetDecimal(26),
						ComponentList = reader.GetDecimal(27)
					};
					bikes.Add(bike);
				}
			}
			return bikes;
		}
	}
}
