﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class RevisionHistoryDataQuery
    {
        public AppDb Db { get; }

        public RevisionHistoryDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<RevisionHistoryData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM REVISIONHISTORY WHERE id = @ID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<RevisionHistoryData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM REVISIONHISTORY ORDER BY ID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM REVISIONHISTORY";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<RevisionHistoryData>> ReadAllAsync(DbDataReader reader)
        {
            var revs = new List<RevisionHistoryData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var rev = new RevisionHistoryData(Db)
                    {
                        ID = reader.GetInt32(0),
                        Version = reader.GetString(1),
                        ChangeDate = reader.GetDateTime(2),
                        Name = reader.GetString(3),
                        RevisionComments = reader.GetString(4)
                    };
                    revs.Add(rev);
                }
            }
            return revs;
        }
    }
}
