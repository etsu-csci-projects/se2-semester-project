﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class RetailStoreDataQuery
    {
        public AppDb Db { get; }

        public RetailStoreDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<RetailStoreData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM RETAILSTORE WHERE id = @STOREID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@STOREID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<RetailStoreData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM RETAILSTORE ORDER BY STOREID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM RETAILSTORE";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<RetailStoreData>> ReadAllAsync(DbDataReader reader)
        {
            var stores = new List<RetailStoreData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var store = new RetailStoreData(Db)
                    {
                        StoreID = reader.GetInt32(0),
                        StoreName = reader.GetString(1),
                        Phone = reader.GetString(2),
                        ContactFirstName = reader.GetString(3),
                        ContactLastName = reader.GetString(4),
                        Address = reader.GetString(5),
                        ZipCode = reader.GetString(6),
                        CityID = reader.GetDecimal(7)
                    };
                    stores.Add(store);
                }
            }
            return stores;
        }
    }
}
