﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class BikeTubesDataQuery
    {
        public AppDb Db { get; }

        public BikeTubesDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<BikeTubesData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM BIKETUBES WHERE id = @SERIALNUMBER";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SERIALNUMBER",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<BikeTubesData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM BIKETUBES ORDER BY SERIALNUMBER DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM BIKETUBES";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<BikeTubesData>> ReadAllAsync(DbDataReader reader)
        {
            var tubes = new List<BikeTubesData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var tube = new BikeTubesData(Db)
                    {
                        SerialNumber = reader.GetInt32(0),
                        TubeName = reader.GetString(1),
                        TubeID = reader.GetInt32(2),
                        Length = reader.GetDouble(3)
                    };
                    tubes.Add(tube);
                }
            }
            return tubes;
        }
    }
}
