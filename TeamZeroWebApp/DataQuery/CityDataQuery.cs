﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;

namespace TeamZeroWebApp.DataQuery
{
    public class CityDataQuery 
    {
        public AppDb Db { get; }

        public CityDataQuery(AppDb db)
        {
            Db = db;
        }

        // Provides syntax for a single SELECT query
        public async Task<CityData> FindOneAsync(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CITY WHERE id = @CITYID";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CITYID",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        // Provides syntax for an indiscriminate SELECT query, returning all values in the table
        public async Task<List<CityData>> LatestPostsAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CITY ORDER BY CITYID DESC LIMIT 10;";
            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }

        // Provides syntax for an indiscriminate DELETE query, clearing the table
        public async Task DeleteAllAsync()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM CITY";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        // TODO: Implement ResetAuto() (Provides syntax to preform an ALTER TABLE query in order to reset the auto incrementation value of a primary key that employs it)

        // Employed internally by the class to read out the returned table entries for the SELECT queries
        private async Task<List<CityData>> ReadAllAsync(DbDataReader reader)
        {
            var cities = new List<CityData>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var city = new CityData(Db)
                    {
                        CityID = reader.GetDecimal(0),
                        ZipCode = reader.GetString(1),
                        City = reader.GetString(2),
                        State = reader.GetString(3),
                        AreaCode = reader.GetString(4),
                        Population1990 = reader.GetDecimal(5),
                        Population1980 = reader.GetDecimal(6),
                        Country = reader.GetString(7),
                        Latitude = reader.GetDouble(8),
                        Longitude = reader.GetDouble(9),
                        PopulationCDF = reader.GetDouble(10)
                    };
                    cities.Add(city);
                }
            }
            return cities;
        }
    }
}
