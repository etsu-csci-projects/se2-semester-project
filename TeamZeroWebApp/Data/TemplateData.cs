﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      TemplateData.cs
/// Description:    
///                 Contains getters and setters for TemplateData class.
///                 TemplateData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
    

    public class TemplateData
    {
        //For each parameter, including the primary key, create a property here

        internal AppDb Db { get; set; }

        public TemplateData()
        {

        }

        internal TemplateData(AppDb db)
        {
            Db = db;
        }

        //public async Task InsertAsync()
        //{
        //    using var cmd = Db.Connection.CreateCommand();
        //    cmd.CommandText = @"INSERT INTO TABLE (PARAM, PARAM) VALUES (@param, @param);";
        //    BindParams(cmd);
        //    await cmd.ExecuteNonQueryAsync();
        //    PK = (PKtype) cmd.LastInsertedId;
        //}

        //public async Task UpdateAsync()
        //{
        //    using var cmd = Db.Connection.CreateCommand();
        //    cmd.CommandText = @"UPDATE TABLE SET PARAM = @param, PARAM = @param WHERE PK = @pk;";
        //    BindParams(cmd);
        //    BindID(cmd);
        //    await cmd.ExecuteNonQueryAsync();
        //}

        //public async Task DeleteAsync()
        //{
        //    using var cmd = Db.Connection.CreateCommand();
        //    cmd.CommandText = @"DELETE FROM TABLE WHERE PK = @pk;";
        //    BindID(cmd);
        //    await cmd.ExecuteNonQueryAsync();
        //}

        //private void BindID(MySqlCommand cmd)
        //{
        //    cmd.Parameters.Add(new MySqlParameter
        //    {
        //        ParameterName = "@pk",
        //        DbType = DbType.PKtype,
        //        Value = PKProperty,
        //    });
        //}

        //private void BindParams(MySqlCommand cmd)
        //{
        //    cmd.Parameters.Add(new MySqlParameter
        //    {
        //        ParameterName = "@param",
        //        DbType = DbType.ParamType,
        //        Value = ParamProperty,
        //    });
        //    
        //    Repeat for each param
        //}


    }
}
