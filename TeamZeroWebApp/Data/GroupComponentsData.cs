﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      GroupComponentsData.cs
/// Description:    
///                 Contains getters and setters for GroupComponentsData class.
///                 GroupComponentsData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class GroupComponentsData
	{
		//For each parameter, including the primary key, create a property here
		public decimal GroupID { get; set; }
		public int ComponentID { get; set; }
		public int SerialNumber { get; set; }
		internal AppDb Db { get; set; }

		public GroupComponentsData()
		{

		}

		internal GroupComponentsData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"INSERT INTO GROUPCOMPONENTS (GroupID, ComponentID) VALUES (@GROUPID, @COMPONENTID);";
		    BindParams(cmd);
		    await cmd.ExecuteNonQueryAsync();
		    SerialNumber = (int) cmd.LastInsertedId;

		}

		public async Task UpdateAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"UPDATE GROUPCOMPONENTS SET GroupID = @GROUPID, ComponentID = @COMPONENTID WHERE SerialNumber = @SERIALNUMBER;";
		    BindParams(cmd);
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"DELETE FROM GROUPCOMPONENTS WHERE SerialNumber = @SERIALNUMBER;";
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
		    cmd.Parameters.Add(new MySqlParameter
		    {
		        ParameterName = "@SERIALNUMBER",
		        DbType = DbType.Int32,
		        Value = SerialNumber,
		    });
		}

		private void BindParams(MySqlCommand cmd)
		{
			
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@GROUPID",
				DbType = DbType.Decimal,
				Value = GroupID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@COMPONENTID",
				DbType = DbType.Int32,
				Value = ComponentID,
			});
		}


	}
}
