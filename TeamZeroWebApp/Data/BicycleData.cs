﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      BicycleData.cs
/// Description:    
///                 Contains getters and setters for BicycleData class.
///                 BicycleData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Thursday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
	public class BicycleData
	{
		public double ChainStay { get; set; }
		public decimal ComponentList { get; set; }
		public string Construction { get; set; }
		public int CustomerID { get; set; }
		public string CustomName { get; set; }
		public int EmployeeID { get; set; }
		public decimal FrameAssembler { get; set; }
		public decimal FramePrice { get; set; }
		public double FrameSize { get; set; }
		public double HeadTubeAngle { get; set; }
		public string LetterStyleID { get; set; }
		public decimal ListPrice { get; set; }
		public string ModelType { get; set; }
		public DateTime OrderDate { get; set; }
		public decimal Painter { get; set; }
		public int PaintID { get; set; }
		public decimal SalePrice { get; set; }
		public string SaleState { get; set; }
		public decimal SalesTax { get; set; }
		public double SeatTubeAngle { get; set; }
		public int SerialNumber { get; set; }
		public DateTime ShipDate { get; set; }
		public decimal ShipEmployee { get; set; }
		public decimal ShipPrice { get; set; }
		public DateTime StartDate { get; set; }
		public int StoreID { get; set; }
		public double TopTube { get; set; }
		public decimal WaterBottleBrazeons { get; set; }

		internal AppDb Db { get; set; }

		public BicycleData()
		{

		}

		public BicycleData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"INSERT INTO BICYCLE (ChainStay, ComponentList, Construction, CustomerID, CustomName, EmployeeID, FrameAssembler, FramePrice, FrameSize, HeadTubeAngle,
            LetterStyleID, ListPrice, ModelType, OrderDate, Painter, PaintID, SalePrice, SaleState, SalesTax, SeatTubeAngle, ShipDate, ShipEmployee, ShipPrice, StartDate, StoreID,
            TopTube, WaterBottleBrazeons) VALUES (@CHAINSTAY, @COMPONENTLIST, @CONSTRUCTION, @CUSTOMERID, @CUSTOMNAME, @EMPLOYEEID, @FRAMEASSEMBLER, @FRAMEPRICE, @FRAMESIZE, @HEADTUBEANGLE,
            @LETTERSTYLEID, @LISTPRICE, @MODELTYPE, @ORDERDATE, @PAINTER, @PAINTID, @SALEPRICE, @SALESTATE, @SALESTAX, @SEATTUBEANGLE, @SHIPDATE, @SHIPEMPLOYEE, @SHIPPRICE, @STARTDATE, @STOREID,
            @TOPTUBE, @WATERBOTTLEBRAZEONS);";
			BindParams(cmd);
			await cmd.ExecuteNonQueryAsync();
			SerialNumber = (int)cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"UPDATE BICYCLE SET ChainStay = @CHAINSTAY, ComponentList = @COMPONENTLIST, Construction = @CONSTRUCTION, CustomerID = @CUSTOMERID, CustomName = @CUSTOMNAME,
            EmployeeID = @EMPLOYEEID, FrameAssembler = @FRAMEASSEMBLER, FramePrice = @FRAMEPRICE, FrameSize = @FRAMESIZE, HeadTubeAngle = @HEADTUBEANGLE, LetterStyleID = @LETTERSTYLEID,
            ListPrice = @LISTPRICE, ModelType = @MODELTYPE, OrderDate = @ORDERDATE, Painter = @PAINTER, PaintID = @PAINTID, SalePrice = @SALEPRICE, SaleState = @SALESTATE, SalesTax = @SALESTAX,
            SeatTubeAngle = @SEATTUBEANGLE, ShipDate = @SHIPDATE, ShipEmployee = @SHIPEMPLOYEE, ShipPrice = @SHIPPRICE, StartDate = @STARTDATE, StoreID = @STOREID,
            TopTube = @TOPTUBE, WaterBottleBrazeons = @WATERBOTTLEBRAZEONS WHERE SerialNumber = @SERIALNUMBER;";
			BindParams(cmd);
			BindID(cmd);
			await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"DELETE FROM BICYCLE WHERE SerialNumber = @SERIALNUMBER;";
			BindID(cmd);
			await cmd.ExecuteNonQueryAsync();
		}


		public override bool Equals(object obj)
		{
			var Dat = obj as BicycleData;
			bool equality = true;
			if (this.ChainStay == Dat.ChainStay && this.ComponentList == Dat.ComponentList && this.Construction == Dat.Construction && this.CustomerID == Dat.CustomerID &&
			   this.CustomName == Dat.CustomName && this.EmployeeID == Dat.EmployeeID && this.FrameAssembler == Dat.FrameAssembler && this.FramePrice == Dat.FramePrice &&
			   this.FrameSize == Dat.FrameSize && this.HeadTubeAngle == Dat.HeadTubeAngle && this.LetterStyleID == Dat.LetterStyleID && this.ListPrice == Dat.ListPrice &&
			   this.ModelType == Dat.ModelType && this.Painter == Dat.Painter && this.PaintID == Dat.PaintID && this.SalePrice == Dat.SalePrice &&
			   this.SaleState == Dat.SaleState && this.SalesTax == Dat.SalesTax && this.SeatTubeAngle == Dat.SeatTubeAngle && this.SerialNumber == Dat.SerialNumber &&
			   this.ShipEmployee == Dat.ShipEmployee && this.ShipPrice == Dat.ShipPrice && this.StoreID == Dat.StoreID &&
			   this.TopTube == Dat.TopTube && this.WaterBottleBrazeons == Dat.WaterBottleBrazeons)
			{
				if (this.OrderDate.Date.CompareTo(Dat.OrderDate.Date) == 0 && this.StartDate.Date.CompareTo(Dat.StartDate.Date) == 0 && this.ShipDate.Date.CompareTo(Dat.ShipDate.Date) == 0)
				{
					equality = true;
				}
				else
				{
					equality = false;
				}
			}
			else
			{
				equality = false;
			}
			return equality;
		}

		private void BindID(MySqlCommand cmd)
		{
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SERIALNUMBER",
				DbType = DbType.Int32,
				Value = SerialNumber,
			});
		}

		private void BindParams(MySqlCommand cmd)
		{
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CHAINSTAY",
				DbType = DbType.Double,
				Value = ChainStay,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@COMPONENTLIST",
				DbType = DbType.Decimal,
				Value = ComponentList,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CONSTRUCTION",
				DbType = DbType.String,
				Value = Construction,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CUSTOMERID",
				DbType = DbType.Int32,
				Value = CustomerID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CUSTOMNAME",
				DbType = DbType.String,
				Value = CustomName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@EMPLOYEEID",
				DbType = DbType.Int32,
				Value = EmployeeID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@FRAMEASSEMBLER",
				DbType = DbType.Decimal,
				Value = FrameAssembler,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@FRAMEPRICE",
				DbType = DbType.Decimal,
				Value = FramePrice,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@FRAMESIZE",
				DbType = DbType.Double,
				Value = FrameSize,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@HEADTUBEANGLE",
				DbType = DbType.Double,
				Value = HeadTubeAngle,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@LETTERSTYLEID",
				DbType = DbType.String,
				Value = LetterStyleID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@LISTPRICE",
				DbType = DbType.Decimal,
				Value = ListPrice,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@MODELTYPE",
				DbType = DbType.String,
				Value = ModelType,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@ORDERDATE",
				DbType = DbType.DateTime,
				Value = OrderDate,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@PAINTER",
				DbType = DbType.Decimal,
				Value = Painter,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@PAINTID",
				DbType = DbType.Int32,
				Value = PaintID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SALEPRICE",
				DbType = DbType.Decimal,
				Value = SalePrice,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SALESTATE",
				DbType = DbType.String,
				Value = SaleState,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SALESTAX",
				DbType = DbType.Decimal,
				Value = SalesTax,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SEATTUBEANGLE",
				DbType = DbType.Double,
				Value = SeatTubeAngle,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SHIPDATE",
				DbType = DbType.DateTime,
				Value = ShipDate,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SHIPEMPLOYEE",
				DbType = DbType.Decimal,
				Value = ShipEmployee,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SHIPPRICE",
				DbType = DbType.Decimal,
				Value = ShipPrice,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@STARTDATE",
				DbType = DbType.DateTime,
				Value = StartDate,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@STOREID",
				DbType = DbType.Int32,
				Value = StoreID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@TOPTUBE",
				DbType = DbType.Double,
				Value = TopTube,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@WATERBOTTLEBRAZEONS",
				DbType = DbType.Decimal,
				Value = WaterBottleBrazeons,
			});
		}

		//public override bool Equals(object obj)
		//{
		//	return obj is BicycleData data &&
		//		   ChainStay == data.ChainStay &&
		//		   ComponentList == data.ComponentList &&
		//		   Construction == data.Construction &&
		//		   CustomerID == data.CustomerID &&
		//		   CustomName == data.CustomName &&
		//		   EmployeeID == data.EmployeeID &&
		//		   FrameAssembler == data.FrameAssembler &&
		//		   FramePrice == data.FramePrice &&
		//		   FrameSize == data.FrameSize &&
		//		   HeadTubeAngle == data.HeadTubeAngle &&
		//		   LetterStyleID == data.LetterStyleID &&
		//		   ListPrice == data.ListPrice &&
		//		   ModelType == data.ModelType &&
		//		   OrderDate == data.OrderDate &&
		//		   Painter == data.Painter &&
		//		   PaintID == data.PaintID &&
		//		   SalePrice == data.SalePrice &&
		//		   SaleState == data.SaleState &&
		//		   SalesTax == data.SalesTax &&
		//		   SeatTubeAngle == data.SeatTubeAngle &&
		//		   SerialNumber == data.SerialNumber &&
		//		   ShipDate == data.ShipDate &&
		//		   ShipEmployee == data.ShipEmployee &&
		//		   ShipPrice == data.ShipPrice &&
		//		   StartDate == data.StartDate &&
		//		   StoreID == data.StoreID &&
		//		   TopTube == data.TopTube &&
		//		   WaterBottleBrazeons == data.WaterBottleBrazeons;
		//}


	}
}
