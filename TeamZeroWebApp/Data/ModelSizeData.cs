﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      ModelSizeData.cs
/// Description:    
///                 Contains getters and setters for ModelSizeData class.
///                 ModelSizeData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class ModelSizeData
	{
		//For each parameter, including the primary key, create a property here

		internal AppDb Db { get; set; }
		public string ModelType { get; set; }
		public double MSize { get; set; }
		public double TopTube { get; set; }
		public double ChainStay { get; set; }
		public double TotalLength { get; set; }
		public double GroundClearance { get; set; }
		public double HeadTubeAngle { get; set; }
		public double SeatTubeAngle { get; set; }
		public int SerialNumber { get; set; }

		public ModelSizeData()
		{

		}

		internal ModelSizeData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"INSERT INTO MODELSIZE (ModelType, TopTube, ChainStay, TotalLength, GroundClearance, HeadTubeAngle, SeatTubeAngle) VALUES (@MODELTYPE, @TOPTUBE, @CHAINSTAY, @TOTALLENGTH, @GROUNDCLEARANCE, @HEADTUBEANGLE, @SEATTUBEANGLE);";
		    BindParams(cmd);
		    await cmd.ExecuteNonQueryAsync();
		    SerialNumber = (int) cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"UPDATE MODELSIZE SET ModelType = @MODELTYPE, MSize = @MISIZE, TopTube = @TOPTUBE, ChainStay = @CHAINSTAY, TotalLength = @TOTALLENGTH, GroundClearance = @GROUNDCLEARANCE, HeadTubeAngle = @HEADTUBEANGLE, SeatTubeAngle = @SEATTUBEANGLE WHERE SerialNumber = @SERIALNUMBER;";
		    BindParams(cmd);
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"DELETE FROM MODELSIZE WHERE SerialNumber = @SERIALNUMBER;";
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
		    cmd.Parameters.Add(new MySqlParameter
		    {
		        ParameterName = "@SERIALNUMBER",
		        DbType = DbType.Int32,
		        Value = SerialNumber,
		    });
		}

		private void BindParams(MySqlCommand cmd)
		{
			//    cmd.Parameters.Add(new MySqlParameter
			//    {
			//        ParameterName = "@param",
			//        DbType = DbType.ParamType,
			//        Value = ParamProperty,
			//    });
			//    
			//    Repeat for each param
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@MODELTYPE",
				DbType = DbType.String,
				Value = ModelType,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@MSIZE",
				DbType = DbType.Decimal,
				Value = MSize,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@TOPTUBE",
				DbType = DbType.Double,
				Value = TopTube,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CHAINSTAY",
				DbType = DbType.Double,
				Value = ChainStay,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@TOTALLENGTH",
				DbType = DbType.Double,
				Value = TotalLength,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@GROUNDCLEARANCE",
				DbType = DbType.Double,
				Value = GroundClearance,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@HEADTUBEANGLE",
				DbType = DbType.Double,
				Value = HeadTubeAngle,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SEATTUBEANGLE",
				DbType = DbType.Double,
				Value = SeatTubeAngle,
			});
		}


	}
}
