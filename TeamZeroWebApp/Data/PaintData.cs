﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      PaintData.cs
/// Description:    
///                 Contains getters and setters for PaintData class.
///                 PaintData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class PaintData
	{
		//For each parameter, including the primary key, create a property here
		public decimal PaintID { get; set; }//PK
		public string ColorName { get; set; }
		public string ColorStyle { get; set; }
		public string ColorList { get; set; }
		public DateTime DateIntroduced { get; set; }
		public DateTime DateDiscontinued { get; set; }

		internal AppDb Db { get; set; }

		public PaintData()
		{

		}

		internal PaintData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"INSERT INTO PAINT (ColorName, ColorSet, Colorist, DateIntroduced, DateDiscontinued) VALUES (@COLORNAME, @COLORSET, @COLORIST, @DATEINTRODUCED, @DATEDISCONTINUED);";
		    BindParams(cmd);
		    await cmd.ExecuteNonQueryAsync();
		    PaintID = (decimal) cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"UPDATE PAINT SET ColorName = @COLORNAME, ColorSet = @COLORSET, Colorist = @COLORIST, DateIntroduced = @DATEINTRODUCED, DateDiscontinued = DATEDISCONTINUED WHERE PaintID = @PAINTID;";
		    BindParams(cmd);
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"DELETE FROM PAINT WHERE PaintID = @PAINTID;";
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
		    cmd.Parameters.Add(new MySqlParameter
		    {
		        ParameterName = "@PAINTID",
		        DbType = DbType.Decimal,
		        Value = PaintID,
		    });
		}

		private void BindParams(MySqlCommand cmd)
		{
			//    cmd.Parameters.Add(new MySqlParameter
			//    {
			//        ParameterName = "@param",
			//        DbType = DbType.ParamType,
			//        Value = ParamProperty,
			//    });
			//    
			//    Repeat for each param
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@COLORNAME",
				DbType = DbType.String,
				Value = ColorName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@COLORSTYLE",
				DbType = DbType.String,
				Value = ColorStyle,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@COLORLIST",
				DbType = DbType.String,
				Value = ColorList,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DATEINTRODUCED",
				DbType = DbType.DateTime,
				Value = DateIntroduced,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DATEDISCONTINUED",
				DbType = DbType.DateTime,
				Value = DateDiscontinued,
			});
		}


	}
}
