﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      PurchaseOrderData.cs
/// Description:    
///                 Contains getters and setters for PurchaseOrderData class.
///                 PurchaseOrderData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
    

    public class PurchaseOrderData
    {
        //For each parameter, including the primary key, create a property here
        public int PurchaseID { get; set; }
        public int EmployeeID { get; set; }
        public decimal ManufacturerID { get; set; }
        public decimal TotalList { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal Discount { get; set; }
        public DateTime OrderDate{ get; set; }
        public DateTime ReceiveDate { get; set; }
        public decimal AmountDue { get; set; }

        internal AppDb Db { get; set; }

        public PurchaseOrderData()
        {

        }

        internal PurchaseOrderData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO PURCHASEORDER (EmployeeID, ManufacturerID, TotalList, ShippingCost, Discount, OrderDate, ReceiveDate, AmountDue) VALUES (@PURCHASEID, @EMPLOYEEID, @MANUFACTURERID, @TOTALLIST, @SHIPPINGCOST, @DISCOUNT, @ORDERDATE, @RECEIVEDATE, @AMOUNTDUE);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            PurchaseID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE PURCHASEORDER SET EmployeeID = @EMPLOYEEID, ManufacturerID = @MANUFACTURERID, TotalList = @TOTALLIST, ShippingCost = @SHIPPINGCOST, Discount = @DISCOUNT, OrderDate = @ORDERDATE, ReceiveDate = @RECEIVEDATE, AmountDue = @AMOUNTDUE WHERE PurchaseID = @PURCHASEID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM PURCHASEORDER WHERE PurchaseID = @PURCHASEID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PURCHASEID",
                DbType = DbType.Int32,
                Value = PurchaseID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@EMPLOYEEID",
                DbType = DbType.Int32,
                Value = EmployeeID,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@MANUFACTURERID",
                DbType = DbType.Decimal,
                Value = ManufacturerID,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@TOTALLIST",
                DbType = DbType.Decimal,
                Value = TotalList,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SHIPPINGCOST",
                DbType = DbType.Decimal,
                Value = ShippingCost,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@DISCOUNT",
                DbType = DbType.Decimal,
                Value = Discount,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ORDERDATE",
                DbType = DbType.DateTime,
                Value = OrderDate,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@RECEIVEDATE",
                DbType = DbType.DateTime,
                Value = ReceiveDate,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@AMOUNTDUE",
                DbType = DbType.Decimal,
                Value = AmountDue,
            });
        }
    }
}
