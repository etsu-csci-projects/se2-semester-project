﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      GroupoData.cs
/// Description:    
///                 Contains getters and setters for GroupoData class.
///                 GroupoData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class GroupoData
	{
		//For each parameter, including the primary key, create a property here
		public decimal ComponentGroupID { get; set; }
		public string GroupName { get; set; }
		public string BikeType { get; set; }
		public decimal Year { get; set; }
		public decimal EndYear { get; set; }
		public decimal Weight { get; set; }
		public int SerialNumber { get; set; }

		internal AppDb Db { get; set; }

		public GroupoData()
		{

		}

		internal GroupoData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"INSERT INTO GROUPO (ComponentGroupID, GroupName, BikeType, Year, EndYear, Weight) VALUES (@COMPONENTGROUPID, @GROUPNAME, @BIKETYPE, @YEAR, @ENDYEAR, @WEIGHT);";
		    BindParams(cmd);
		    await cmd.ExecuteNonQueryAsync();
		    SerialNumber = (int) cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"UPDATE GROUPO SET ComponentGroupID = @COMPONENTGROUPID, GroupName = @GROUPNAME, BikeType = @BIKETYPE, Year = @YEAR, EndYear = @ENDYEAR, Weight = @WEIGHT WHERE SerialNumber = @SERIALNUMBER;";
		    BindParams(cmd);
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"DELETE FROM GROUPO WHERE SerialNumber = @SERIALNUMBER;";
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
		    cmd.Parameters.Add(new MySqlParameter
		    {
		        ParameterName = "@SERIALNUMBER",
		        DbType = DbType.Int32,
		        Value = SerialNumber,
		    });
		}

		private void BindParams(MySqlCommand cmd)
		{
			//cmd.Parameters.Add(new MySqlParameter
			//{
			//    ParameterName = "@param",
			//    DbType = DbType.ParamType,
			//    Value = ParamProperty,
			//});

			//Repeat for each param
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@COMPONENTGROUPID",
				DbType = DbType.Decimal,
				Value = ComponentGroupID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@GROUPNAME",
				DbType = DbType.String,
				Value = GroupName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@BIKETYPE",
				DbType = DbType.String,
				Value = BikeType,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@YEAR",
				DbType = DbType.Decimal,
				Value = Year,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@ENDYEAR",
				DbType = DbType.Decimal,
				Value = EndYear,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@WEIGHT",
				DbType = DbType.Decimal,
				Value = Weight,
			});

		}


	}
}
