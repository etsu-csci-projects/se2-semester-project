﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      CustomerData.cs
/// Description:    
///                 Contains getters and setters for CustomerData class.
///                 CustomerData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
    

    public class CustomerData
    {
        public string Address { get; set; }
        public decimal BalanceDue { get; set; }
        public decimal CityID { get; set; }
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string ZipCode { get; set; }

        internal AppDb Db { get; set; }

        public CustomerData()
        {

        }

        internal CustomerData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO CUSTOMER (Address, BalanceDue, CityID, FirstName, LastName, Phone, ZipCode) 
            VALUES (@ADDRESS, @BALANCEDUE, @CITYID, @FIRSTNAME, @LASTNAME, @PHONE, @ZIPCODE);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            CustomerID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE CUSTOMER SET Address = @ADDRESS, BalanceDue = @BALANCEDUE, CityID = @CITYID, FirstName = @FIRSTNAME, LastName = @LASTNAME,
            Phone = @PHONE, ZipCode = @ZIPCODE WHERE CustomerID = @CUSTOMERID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM CUSTOMER WHERE CustomerID = @CUSTOMERID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CUSTOMERID",
                DbType = DbType.Int32,
                Value = CustomerID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ADDRESS",
                DbType = DbType.String,
                Value = Address,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@BALANCEDUE",
                DbType = DbType.Decimal,
                Value = BalanceDue,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CITYID",
                DbType = DbType.Decimal,
                Value = CityID,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@FIRSTNAME",
                DbType = DbType.String,
                Value = FirstName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LASTNAME",
                DbType = DbType.String,
                Value = LastName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PHONE",
                DbType = DbType.String,
                Value = Phone,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ZIPCODE",
                DbType = DbType.String,
                Value = ZipCode,
            });
        }


    }
}
