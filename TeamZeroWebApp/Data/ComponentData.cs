﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      ComponentData.cs
/// Description:    
///                 Contains getters and setters for ComponentData class.
///                 ComponentData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
    

    public class ComponentData
    {
        public string Category { get; set; }
        public int ComponentID { get; set; }
        public string Description { get; set; }
        public decimal EndYear { get; set; }
        public decimal EstimatedCost { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }
        public decimal ListPrice { get; set; }
        public decimal ManufacturerID { get; set; }
        public string ProductNumber { get; set; }
        public double QuantityOnHand { get; set; }
        public string Road { get; set; }
        public double Weight { get; set; }
        public double Width { get; set; }
        public decimal Year { get; set; }

        internal AppDb Db { get; set; }

        public ComponentData()
        {

        }

        internal ComponentData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO COMPONENT (Category, Description, EndYear, EstimatedCost, Height, Length, ListPrice, ManufacturerID, ProductNumber, QuantityOnHand,
            Road, Weight, Width, Year) VALUES (@CATEGORY, @DESCRIPTION, @ENDYEAR, @ESTIMATEDCOST, @HEIGHT, @LENGTH, @LISTPRICE, @MANUFACTURERID, @PRODUCTNUMBER, @QUANTITYONHAND,
            @ROAD, @WEIGHT, @WIDTH, @YEAR);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            ComponentID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE COMPONENT SET Category = @CATEGORY, Description = @DESCRIPTION, EndYear = @ENDYEAR, EstimatedCost = @ESTIMATECOST
            Height = @HEIGHT, Length = @LENGTH, ListPrice = @LISTPRICE, ManufacturerID = @MANUFACTURERID, ProductNumber = @PRODUCTNUMBER, QuantityOnHand = @QUANTITYONHAND,
            Road = @ROAD, Weight = @WEIGHT, Width = @WIDTH, Year = @YEAR WHERE ComponentID = @COMPONENTID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM COMPONENT WHERE ComponentID = @COMPONENTID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@COMPONENTID",
                DbType = DbType.Int32,
                Value = ComponentID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CATEGORY",
                DbType = DbType.String,
                Value = Category,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@DESCRIPTION",
                DbType = DbType.String,
                Value = Description,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ENDYEAR",
                DbType = DbType.Decimal,
                Value = EndYear,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ESTIMATEDCOST",
                DbType = DbType.Decimal,
                Value = EstimatedCost,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@HEIGHT",
                DbType = DbType.Double,
                Value = Height,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LENGTH",
                DbType = DbType.Double,
                Value = Length,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LISTPRICE",
                DbType = DbType.Decimal,
                Value = ListPrice,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@MANUFACTURERID",
                DbType = DbType.Decimal,
                Value = ManufacturerID,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PRODUCTNUMBER",
                DbType = DbType.String,
                Value = ProductNumber,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@QUANTITYONHAND",
                DbType = DbType.Double,
                Value = QuantityOnHand,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ROAD",
                DbType = DbType.String,
                Value = Road,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@WEIGHT",
                DbType = DbType.Double,
                Value = Weight,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@WIDTH",
                DbType = DbType.Double,
                Value = Width,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@YEAR",
                DbType = DbType.Decimal,
                Value = Year,
            });
        }


    }
}
