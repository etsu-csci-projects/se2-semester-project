﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      LetterStyleData.cs
/// Description:    
///                 Contains getters and setters for LetterStyleData class.
///                 LetterStyleData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class LetterStyleData
	{
		//For each parameter, including the primary key, create a property here
		public string LetterStyle { get; set; }
		public string Description { get; set; }
		public int SerialNumber { get; set; }
		internal AppDb Db { get; set; }

		public LetterStyleData()
		{

		}

		internal LetterStyleData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"INSERT INTO LETTERSTYLE (LetterStyle, Description) VALUES (@LETTERSTYLE, @DESCRIPTION);";
			BindParams(cmd);
			await cmd.ExecuteNonQueryAsync();
			SerialNumber = (int) cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"UPDATE LETTERSTYLE SET LetterStyle = @LETTERSTYLE, Description = @DESCRIPTION WHERE SerialNumber = @SERIALNUMBER;";
			BindParams(cmd);
			BindID(cmd);
			await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"DELETE FROM LETTERSTYLE WHERE SerialNumber = @SERIALNUMBER;";
			BindID(cmd);
			await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SERIALNUMBER",
				DbType = DbType.Int32,
				Value = SerialNumber,
			});
		}

		private void BindParams(MySqlCommand cmd)
		{
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@LETTERSTYLE",
				DbType = DbType.String,
				Value = LetterStyle,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DESCRIPTION",
				DbType = DbType.String,
				Value = Description,
			});
		}


	}
}
