﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      RevisionHistoryData.cs
/// Description:    
///                 Contains getters and setters for RevisionHistoryData class.
///                 RevisionHistoryData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
   

    public class RevisionHistoryData
    {
        //For each parameter, including the primary key, create a property here
        public int ID { get; set; }
        public string Version { get; set; }
        public DateTime ChangeDate { get; set; }
        public string Name { get; set; }
        public string RevisionComments { get; set; }

        internal AppDb Db { get; set; }

        public RevisionHistoryData()
        {

        }

        internal RevisionHistoryData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO REVISIONHISTORY (Version, ChangeDate, Name, RevisionComments) VALUES (@VERSION, @CHANGEDATE, @NAME, @REVISIONCOMMENTS);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            ID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE REVISIONHISTORY SET Version = @VERSION, ChangeDate = @CHANGEDATE, Name = @NAME, RevisionComments = @REVISIONCOMMENTS WHERE ID = @ID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM REVISIONHISTORY WHERE ID = @ID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ID",
                DbType = DbType.Int32,
                Value = ID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@VERSION",
                DbType = DbType.String,
                Value = Version,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CHANGEDATE",
                DbType = DbType.DateTime,
                Value = ChangeDate,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@NAME",
                DbType = DbType.String,
                Value = Name,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@REVISIONCOMMENTS",
                DbType = DbType.String,
                Value = RevisionComments,
            });
        }
    }
}
