﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      SampleNameData.cs
/// Description:    
///                 Contains getters and setters for SampleNameData class.
///                 SampleNameData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
    

    public class SampleNameData
    {
        //For each parameter, including the primary key, create a property here
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }

        internal AppDb Db { get; set; }

        public SampleNameData()
        {

        }

        internal SampleNameData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO SAMPLENAME (LastName, FirstName, Gender) VALUES (@FIRSTNAME, @LASTNAME, @GENDER);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            ID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE SAMPLENAME SET LastName = @LASTNAME, FirstName = @FIRSTNAME, Gender = @GENDER WHERE ID = @ID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM SAMPLENAME WHERE ID = @ID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ID",
                DbType = DbType.Int32,
                Value = ID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LASTNAME",
                DbType = DbType.String,
                Value = LastName,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@FIRSTNAME",
                DbType = DbType.String,
                Value = FirstName,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@GENDER",
                DbType = DbType.String,
                Value = Gender,
            });
        }
    }
}
