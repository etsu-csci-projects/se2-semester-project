﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      BicycleTubeUsageData.cs
/// Description:    
///                 Contains getters and setters for BicycleTubeUsageData class.
///                 BicycleTubeUsageData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
    

    public class BicycleTubeUsageData
    {
        public double Quantity { get; set; }
        public decimal SerialNumber { get; set; }
        public int TubeID { get; set; }

        internal AppDb Db { get; set; }

        public BicycleTubeUsageData()
        {
    
        }

        internal BicycleTubeUsageData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO BICYCLETUBEUSAGE (Quantity, TubeID) VALUES (@QUANTITY, @TUBEID);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            SerialNumber = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE BICYCLETUBEUSAGE SET Quantity = @QUANTITY, TubeID = @TUBEID WHERE SerialNumber = @SERIALNUMBER;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM BICYCLETUBEUSAGE WHERE SerialNumber = @SERIALNUMBER;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SERIALNUMBER",
                DbType = DbType.Decimal,
                Value = SerialNumber,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@QUANTITY",
                DbType = DbType.Double,
                Value = Quantity,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@TUBEID",
                DbType = DbType.Int32,
                Value = TubeID,
            });
        }


    }
}
