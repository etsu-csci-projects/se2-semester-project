﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      CityData.cs
/// Description:    
///                 Contains getters and setters for CityData class.
///                 CityData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
    

    public class CityData
    {
        public string AreaCode { get; set; }
        public string City { get; set; }
        public decimal CityID { get; set; }
        public string Country { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public decimal Population1980 { get; set; }
        public decimal Population1990 { get; set; }
        public double PopulationCDF { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        internal AppDb Db { get; set; }

        public CityData()
        {
    
        }

        internal CityData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO CITY (AreaCode, City, Country, Latitude, Longitude, Population1980, Population1990, PopulationCDF, State, ZipCode) 
            VALUES (@AREACODE, @CITY, @COUNTRY, @LATITUDE, @LONGITUDE, @POPULATION1980, @POPULATION1990, @POPULATIONCDF, @STATE, @ZIPCODE);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            CityID = (decimal) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE CITY SET AreaCode = @AREACODE, City = @CITY, Country = @COUNTRY, Latitude = @LATITUDE, Population1980 = @POPULATION1980, Population1990 = @POPULATION1990,
            PopulationCDF = @POPULATIONCDF, State = @STATE, ZipCode = @ZIPCODE WHERE CityID = @CITYID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM CITY WHERE CityID = @CITYID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CITYID",
                DbType = DbType.Decimal,
                Value = CityID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@AREACODE",
                DbType = DbType.String,
                Value = AreaCode,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CITY",
                DbType = DbType.String,
                Value = City,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@COUNTRY",
                DbType = DbType.String,
                Value = Country,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LATITUDE",
                DbType = DbType.Double,
                Value = Latitude,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LONGITUDE",
                DbType = DbType.Double,
                Value = Longitude,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@POPULATION1980",
                DbType = DbType.Decimal,
                Value = Population1980,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@POPULATION1990",
                DbType = DbType.Decimal,
                Value = Population1990,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@POPULATIONCDF",
                DbType = DbType.Double,
                Value = PopulationCDF,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@STATE",
                DbType = DbType.String,
                Value = State,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ZIPCODE",
                DbType = DbType.String,
                Value = ZipCode,
            });
        }


    }
}
