﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      BikePartsData.cs
/// Description:    
///                 Contains getters and setters for BikePartsData class.
///                 BikePartsData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
   

    public class BikePartsData
    {
        public int ComponentID { get; set; }
        public DateTime DateInstalled { get; set; }
        public int EmployeeID { get; set; }
        public string Location { get; set; }
        public decimal Quantity { get; set; }
        public decimal SerialNumber { get; set; }
        public decimal SubstituteID { get; set; }

        internal AppDb Db { get; set; }

        public BikePartsData()
        {
    
        }

        internal BikePartsData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO BIKEPARTS (ComponentID, DateInstalled, EmployeeID, Location, Quantity, SubstituteID) 
            VALUES (@COMPONENTID, @DATEINSTALLED, @EMPLOYEEID, @LOCATION, @QUANTITY, @SUBSTITUTEID);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            SerialNumber = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE BIKEPARTS SET ComponentID = @COMPONENTID, DateInstalled = @DATEINSTALLED, EmployeeID = @EMPLOYEEID, Location = @LOCATION,
            Quantity = @QUANTITY, SubstituteID = @SUBSTITUTEID WHERE SerialNumber = @SERIALNUMBER;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM BIKEPARTS WHERE SerialNumber = @SERIALNUMBER;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SERIALNUMBER",
                DbType = DbType.Decimal,
                Value = SerialNumber,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@COMPONENTID",
                DbType = DbType.Int32,
                Value = ComponentID,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@DATEINSTALLED",
                DbType = DbType.DateTime,
                Value = DateInstalled,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@EMPLOYEEID",
                DbType = DbType.Int32,
                Value = EmployeeID,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LOCATION",
                DbType = DbType.String,
                Value = Location,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@QUANTITY",
                DbType = DbType.Decimal,
                Value = Quantity,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SUBSTITUTEID",
                DbType = DbType.Decimal,
                Value = SubstituteID,
            });
        }


    }
}
