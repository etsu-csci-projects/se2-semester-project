﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      ManufacturerData.cs
/// Description:    
///                 Contains getters and setters for ManufacturerData class.
///                 ManufacturerData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class ManufacturerData
	{
		//For each parameter, including the primary key, create a property here

		internal AppDb Db { get; set; }
		public decimal ManufacturerID { get; set; }
		public string ManufacturerName { get; set; }
		public string ContactName { get; set; }
		public string Phone { get; set; }
		public string Address { get; set; }
		public string ZipCode { get; set; }
		public decimal CityID { get; set; }
		public decimal BalanceDue { get; set; }

		public ManufacturerData()
		{

		}

		internal ManufacturerData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"INSERT INTO MANUFACTURER (ManufacturerName, ContactName, Phone, Address, ZipCode, CityID, BalanceDue) VALUES (@MANUFACTURERNAME, @CONTACTNAME, @PHONE, @ADDRESS, @ZIPCODE, @CITYID, @BALANCEDUE);";
		    BindParams(cmd);
		    await cmd.ExecuteNonQueryAsync();
		    ManufacturerID = (decimal) cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"UPDATE MANUFACTURER SET ManufacturerName = @MANUFACTURERNAME, ContactName = @CONTACTNAME, Phone = @PHONE, Address = @ADDRESS, ZipCode = @ZIPCODE, CityID = @CITYID, BalanceDue = @BALANCEDUE WHERE ManufacturerID = @MANUFACTURERID;";
		    BindParams(cmd);
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"DELETE FROM MANUFACTURER WHERE ManufacturerID = @MANUFACTURERID;";
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
		    cmd.Parameters.Add(new MySqlParameter
		    {
		        ParameterName = "@MANUFACTURERID",
		        DbType = DbType.Decimal,
		        Value = ManufacturerID,
		    });
		}

		private void BindParams(MySqlCommand cmd)
		{
			//    cmd.Parameters.Add(new MySqlParameter
			//    {
			//        ParameterName = "@param",
			//        DbType = DbType.ParamType,
			//        Value = ParamProperty,
			//    });
			//    
			//    Repeat for each param
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@MANUFACTURERNAME",
				DbType = DbType.String,
				Value = ManufacturerName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CONTACTNAME",
				DbType = DbType.String,
				Value = ContactName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@PHONE",
				DbType = DbType.String,
				Value = Phone,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@ADDRESS",
				DbType = DbType.String,
				Value = Address,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@ZIPCODE",
				DbType = DbType.String,
				Value = ZipCode,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CITYID",
				DbType = DbType.Decimal,
				Value = CityID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@BALANCEDUE",
				DbType = DbType.Decimal,
				Value = BalanceDue,
			});
		}


	}
}
