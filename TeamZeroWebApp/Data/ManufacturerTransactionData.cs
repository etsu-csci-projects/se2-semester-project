﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      ManufacturerTransactionData.cs
/// Description:    
///                 Contains getters and setters for ManufacturerTransactionData class.
///                 ManufacturerTransactionData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class ManufacturerTransactionData
	{
		//For each parameter, including the primary key, create a property here

		internal AppDb Db { get; set; }
		public decimal ManufacturerID { get; set; }
		public DateTime TransactionDate { get; set; }
		public decimal EmployeeID { get; set; }
		public decimal Amount { get; set; }
		public string Description { get; set; }
		public decimal Reference { get; set; }
		public int SerialNumber { get; set; }

		public ManufacturerTransactionData()
		{

		}

		internal ManufacturerTransactionData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"INSERT INTO MANUFACTURERTRANSACTION (TransactionDate, EmployeeID, Amount, Description, Reference) VALUES (@TRANSACTIONDATE, @EMPLOYEEID, AMOUNT, DESCRIPTION, REFERENCE);";
		    BindParams(cmd);
		    await cmd.ExecuteNonQueryAsync();
		    SerialNumber = (int) cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"UPDATE MANUFACTURERTRANSACTION SET ManufacturerID = @MANUFACTURERID, TransactionDate = @TRANSACTIONDATE, 
			EmployeeID = @EMPLOYEEID, Amount = @AMOUNT, Description = @DESCRIPTION, Reference = @REFERENCE WHERE SerialNumber = @SERIALNUMBER;";
		    BindParams(cmd);
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
		    using var cmd = Db.Connection.CreateCommand();
		    cmd.CommandText = @"DELETE FROM MANUFACTURERTRANSACTION WHERE SerialNumber = @SERIALNUMBER;";
		    BindID(cmd);
		    await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
		    cmd.Parameters.Add(new MySqlParameter
		    {
		        ParameterName = "@SERIALNUMBER",
		        DbType = DbType.Int32,
		        Value = SerialNumber,
		    });
		}

		private void BindParams(MySqlCommand cmd)
		{
			//    cmd.Parameters.Add(new MySqlParameter
			//    {
			//        ParameterName = "@param",
			//        DbType = DbType.ParamType,
			//        Value = ParamProperty,
			//    });
			//    
			//    Repeat for each param
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@MANUFACTURERID",
				DbType = DbType.Decimal,
				Value = ManufacturerID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@TRANSACTIONDATE",
				DbType = DbType.DateTime,
				Value = TransactionDate,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@EMPLOYEEID",
				DbType = DbType.Decimal,
				Value = EmployeeID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@AMOUNT",
				DbType = DbType.Decimal,
				Value = Amount,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DESCRIPTION",
				DbType = DbType.String,
				Value = Description,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@REFERENCE",
				DbType = DbType.Decimal,
				Value = Reference,
			});
		}


	}
}
