﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      RetailStoreData.cs
/// Description:    
///                 Contains getters and setters for RetailStoreData class.
///                 RetailStoreData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
   

    public class RetailStoreData
    {
        //For each parameter, including the primary key, create a property here
        public int StoreID { get; set; }
        public string StoreName { get; set; }
        public string Phone { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public decimal CityID { get; set; }

        internal AppDb Db { get; set; }

        public RetailStoreData()
        {

        }

        internal RetailStoreData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO RETAILSTORE (StoreName, Phone, ContactFirstName, ContactLastName, Address, ZipCode, CityID) VALUES (@STORENAME, @PHONE, @CONTACTFIRSTNAME, @CONTACTLASTNAME, @ADDRESS, @ZIPCODE, @CITYID);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            StoreID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE RETAILSTORE SET StoreName = @STORENAME, Phone = @PHONE, ContactFirstName = @CONTACTFIRSTNAME, ContactLastName = @CONTACTLASTNAME, Address = @ADDRESS, ZipCode = @ZIPCODE, CityID = @CITYID WHERE StoreID = @STOREID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM RETAILSTORE WHERE StoreID = @STOREID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@STOREID",
                DbType = DbType.Int32,
                Value = StoreID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@STORENAME",
                DbType = DbType.String,
                Value = StoreName,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PHONE",
                DbType = DbType.String,
                Value = Phone,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CONTACTFIRSTNAME",
                DbType = DbType.String,
                Value = ContactFirstName,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CONTACTLASTNAME",
                DbType = DbType.String,
                Value = ContactLastName,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ADDRESS",
                DbType = DbType.String,
                Value = Address,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ZIPCODE",
                DbType = DbType.String,
                Value = ZipCode,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CITYID",
                DbType = DbType.Decimal,
                Value = CityID,
            });
        }
    }
}
