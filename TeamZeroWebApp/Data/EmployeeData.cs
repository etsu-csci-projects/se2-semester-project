﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      EmployeeData.cs
/// Description:    
///                 Contains getters and setters for EmployeeData class.
///                 EmployeeData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
   

    public class EmployeeData
    {
        //For each parameter, including the primary key, create a property here
        public int EmployeeID { get; set; }
		public string TaxPayerID { get; set; }
		public string LastName { get; set; }
		public string FirstName { get; set; }
		public string HomePhone { get; set; }
		public string Address { get; set; }
		public string ZipCode { get; set; }
		public decimal CityID { get; set; }
		public DateTime DateHired { get; set; }
		public DateTime DateReleased { get; set; }
		public decimal CurrentManager { get; set; }
		public decimal SalaryGrade { get; set; }
		public decimal Salary { get; set; }
		public string Title { get; set; }
		public string WorkArea { get; set; }

		internal AppDb Db { get; set; }

        public EmployeeData()
        {

        }

        internal EmployeeData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO EMPLOYEE (TaxPayerID, LastName, FirstName, HomePhone, Address, ZipCode, CityID, DateHired, DateReleased, CurrentManager, SalaryGrade, Salary, Title, WorkArea) 
                                VALUES (@TAXPAYERID, @LASTNAME, @FIRSTNAME, @HOMEPHONE, @ADDRESS, @ZIPCODE, @CITYID, @DATEHIRED, @DATERELEASED, @CURRENTMANAGER, @SALARYGRADE, @SALARY, @TITLE, @WORKAREA);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            EmployeeID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE EMPLOYEE SET TaxPayerID = @TAXPAYERID, LastName = @LASTNAME, FirstName = @FIRSTNAME, HomePhone = @HOMEPHONE, Address = @ADDRESS, ZipCode = @ZIPCODE, CityID = @CITYID,
                                DateHired = @DATEHIRED, DateReleased = @DATERELEASED, CurrentManager = @CURRENTMANAGER, SalaryGrade = @SALARYGRADE, Salary = @SALARY, Title = @TITLE, WorkArea = @WORKAREA 
                                WHERE EmployeeID = @EMPLOYEEID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM EMPLOYEE WHERE EmployeeID = @EMPLOYEEID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@EMPLOYEEID",
                DbType = DbType.Int32,
                Value = EmployeeID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            //cmd.Parameters.Add(new MySqlParameter
            //{
            //    ParameterName = "@param",
            //    DbType = DbType.ParamType,
            //    Value = ParamProperty,
            //});
            cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@EMPLOYEEID",
				DbType = DbType.Decimal,
				Value = EmployeeID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@TAXPAYERID",
				DbType = DbType.String,
				Value = TaxPayerID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@LASTNAME",
				DbType = DbType.String,
				Value = LastName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@FIRSTNAME",
				DbType = DbType.String,
				Value = FirstName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@HOMEPHONE",
				DbType = DbType.String,
				Value = HomePhone,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@ADDRESS",
				DbType = DbType.String,
				Value = Address,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@ZIPCODE",
				DbType = DbType.String,
				Value = ZipCode,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CITYID",
				DbType = DbType.Decimal,
				Value = CityID,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DATEHIRED",
				DbType = DbType.DateTime,
				Value = DateHired,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DATERELEASED",
				DbType = DbType.DateTime,
				Value = DateReleased,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@CURRENTMANAGER",
				DbType = DbType.Decimal,
				Value = CurrentManager,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SALARYGRADE",
				DbType = DbType.Decimal,
				Value = SalaryGrade,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SALARY",
				DbType = DbType.Decimal,
				Value = Salary,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@TITLE",
				DbType = DbType.String,
				Value = Title,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@WORKAREA",
				DbType = DbType.String,
				Value = WorkArea,
			});
            //Repeat for each param
        }


    }
}
