﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      TubeMaterialData.cs
/// Description:    
///                 Contains getters and setters for TubeMaterialData class.
///                 TubeMaterialData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{
   

    public class TubeMaterialData
    {
        //For each parameter, including the primary key, create a property here
        public int TubeID { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public double Diameter { get; set; }
        public double Thickness { get; set; }
        public string Roundness { get; set; }
        public double Weight { get; set; }
        public double Stiffness { get; set; }
        public decimal ListPrice { get; set; }
        public string Construction { get; set; }

        internal AppDb Db { get; set; }

        public TubeMaterialData()
        {

        }

        internal TubeMaterialData(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO TUBEMATERIAL (Material, Description, Diameter, Thickness, Roundness, Weight, Stiffness, ListPrice, Construction) VALUES (@MATERIAL, @DESCRIPTION, @DIAMETER, @THICKNESS, @ROUNDNESS, @WEIGHT, @STIFFNESS, @LISTPRICE, @CONSTRUCTION);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            TubeID = (int) cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE TUBEMATERIAL SET Material = @MATERIAL, Description = @DESCRIPTION, Diameter = @DIAMETER, Thickness = @THICKNESS, Roundness = @ROUNDNESS, Weight = @WEIGHT, Stiffness = @STIFFNESS, ListPrice = @LISTPRICE, Construction = @CONSTRUCTION WHERE TubeID = @TUBEID;";
            BindParams(cmd);
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM TUBEMATERIAL WHERE TubeID = @TUBEID;";
            BindID(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindID(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@TUBEID",
                DbType = DbType.Int32,
                Value = TubeID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@MATERIAL",
                DbType = DbType.String,
                Value = Material,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@DESCRIPTION",
                DbType = DbType.String,
                Value = Description,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@DIAMETER",
                DbType = DbType.Double,
                Value = Diameter,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@THICKNESS",
                DbType = DbType.Double,
                Value = Thickness,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ROUNDNESS",
                DbType = DbType.String,
                Value = Roundness,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@WEIGHT",
                DbType = DbType.Double,
                Value = Weight,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@STIFFNESS",
                DbType = DbType.Double,
                Value = Stiffness,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@LISTPRICE",
                DbType = DbType.Decimal,
                Value = ListPrice,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CONSTRUCTION",
                DbType = DbType.String,
                Value = Construction,
            });
        }
    }
}
