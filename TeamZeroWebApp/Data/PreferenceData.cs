﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      PreferenceData.cs
/// Description:    
///                 Contains getters and setters for PreferenceData class.
///                 PreferenceData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
///
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using System.Data;

namespace TeamZeroWebApp
{


	public class PreferenceData
	{
		//For each parameter, including the primary key, create a property here
		public string ItemName { get; set; }
		public double Value { get; set; }
		public string Description { get; set; }
		public DateTime DateChanged { get; set; }
		public int SerialNumber { get; set; }

		internal AppDb Db { get; set; }

		public PreferenceData()
		{

		}

		internal PreferenceData(AppDb db)
		{
			Db = db;
		}

		public async Task InsertAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"INSERT INTO PREFERENCE (ItemName, Value, Description, DateChanged) VALUES (@ITEMNAME, @VALUE, @DESCRIPTION, @DATECHANGED);";
			BindParams(cmd);
			await cmd.ExecuteNonQueryAsync();
			SerialNumber = (int) cmd.LastInsertedId;
		}

		public async Task UpdateAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"UPDATE PREFERENCE SET ItemName = @ITEMNAME, Value = @VALUE, Description = @DESCRIPTION, DateChanged = @DATECHANGED WHERE SerialNumber = @SERIALNUMBER;";
			BindParams(cmd);
			BindID(cmd);
			await cmd.ExecuteNonQueryAsync();
		}

		public async Task DeleteAsync()
		{
			using var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"DELETE FROM PREFERENCE WHERE SerialNumber = @SERIALNUMBER;";
			BindID(cmd);
			await cmd.ExecuteNonQueryAsync();
		}

		private void BindID(MySqlCommand cmd)
		{
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@SERIALNUMBER",
				DbType = DbType.Int32,
				Value = SerialNumber,
			});
		}

		private void BindParams(MySqlCommand cmd)
		{
			//    cmd.Parameters.Add(new MySqlParameter
			//    {
			//        ParameterName = "@param",
			//        DbType = DbType.ParamType,
			//        Value = ParamProperty,
			//    });
			//    
			//    Repeat for each param
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@ITEMNAME",
				DbType = DbType.String,
				Value = ItemName,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@VALUE",
				DbType = DbType.Double,
				Value = Value,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DESCRIPTION",
				DbType = DbType.String,
				Value = Description,
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@DATECHANGED",
				DbType = DbType.DateTime,
				Value = DateChanged,
			});
		}


	}
}
