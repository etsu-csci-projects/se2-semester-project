﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      GroupComponentsData.cs
/// Description:    
///                 Contains getters and setters for GroupComponentsData class.
///                 GroupComponentsData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
/// 
/// Sources:		https://mysqlconnector.net/tutorials/net-core-mvc/
///
//////////////////////////////////////////////////////////////////////////


using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamZeroWebApp.DataQuery;

namespace TeamZeroWebApp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CustomerTransactionDataController : ControllerBase
	{
		public AppDb Db { get; }
		public CustomerTransactionDataController(AppDb db)
		{
			Db = db;
		}

		// GET api/blog
        [HttpGet]
        public async Task<IActionResult> GetLatest()
        {
            await Db.Connection.OpenAsync();
            var query = new CustomerTransactionDataQuery(Db);
            var result = await query.LatestPostsAsync();
            return new OkObjectResult(result);
        }

        // GET api/blog/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            await Db.Connection.OpenAsync();
            var query = new CustomerTransactionDataQuery(Db);
            var result = await query.FindOneAsync(id);
            if (result is null)
                return new NotFoundResult();
            return new OkObjectResult(result);
        }

        // POST api/blog
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerTransactionData body)
        {
            await Db.Connection.OpenAsync();
            body.Db = Db;
            await body.InsertAsync();
            return new OkObjectResult(body);
        }

        // PUT api/blog/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOne(int id, [FromBody]CustomerTransactionData body)
        {
            await Db.Connection.OpenAsync();
            var query = new CustomerTransactionDataQuery(Db);
            var result = await query.FindOneAsync(id);
            if (result is null)
                return new NotFoundResult();
            result.SerialNumber = body.SerialNumber;
		    result.CustomerID = body.CustomerID;
			result.TransactionDate = body.TransactionDate;
			result.EmployeeID = body.EmployeeID;
			result.Amount = body.Amount;
			result.Description = body.Description;
			result.Reference = body.Reference; 
            await result.UpdateAsync();
            return new OkObjectResult(result);
        }

        // DELETE api/blog/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOne(int id)
        {
            await Db.Connection.OpenAsync();
            var query = new CustomerTransactionDataQuery(Db);
            var result = await query.FindOneAsync(id);
            if (result is null)
                return new NotFoundResult();
            await result.DeleteAsync();
            return new OkResult();
        }

        // DELETE api/blog
        [HttpDelete]
        public async Task<IActionResult> DeleteAll()
        {
            await Db.Connection.OpenAsync();
            var query = new CustomerTransactionDataQuery(Db);
            await query.DeleteAllAsync();
            return new OkResult();
        }

	}
}
