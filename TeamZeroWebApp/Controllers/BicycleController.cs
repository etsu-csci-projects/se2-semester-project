﻿///////////////////////////////////////////////////////////////////////////
///
/// Project:        Sprint 1
/// File Name:      GroupComponentsData.cs
/// Description:    
///                 Contains getters and setters for GroupComponentsData class.
///                 GroupComponentsData class will contain items inside database.
/// Course:         CSCI 4350 - Software Engineering
/// Authors:        
///                 Darien Roach,   roachda@etsu.edu,   Developer
///                 Alec Hamaker,   hamakera@etsu.edu,  Developer
///                 Ben Higgins,    higginsba@etsu.edu, Developer
///                 Thomas Roberts, robertstj@etsu.edu, Scrum Master
///                 Tucker Shepard, shepardt@etsu.edu,  Product Owner
///                 
/// Created:        Friday, February 12th, 2021
/// 
/// Sources:		https://mysqlconnector.net/tutorials/net-core-mvc/
///
//////////////////////////////////////////////////////////////////////////

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySqlConnector;
using TeamZeroWebApp.DataQuery;

namespace TeamZeroWebApp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BicycleController : ControllerBase
	{
		public AppDb Db { get; }

		public BicycleController(AppDb db)
		{
			Db = db;
		}

		[HttpGet]
		public async Task<IActionResult> GetLatest()
		{
			await Db.Connection.OpenAsync();
            var query = new BicycleDataQuery(Db);
            var result = await query.LatestPostsAsync();
            return new OkObjectResult(result);
		}
		
		[HttpGet("{pageNumber}/{pageSize}")]
		public async Task<IActionResult> GetPage(int pageNumber, int pageSize)
		{
			await Db.Connection.OpenAsync();
            var query = new BicycleDataQuery(Db);
            var result = await query.FindPageAsync(pageNumber, pageSize);
            return new OkObjectResult(result);
		}

		[HttpGet("ListPrice/{pageNumber}/{pageSize}/{ascending}")]
		public async Task<IActionResult> GetPageOrderByListPrice(int pageNumber, int pageSize, bool ascending)
		{
			await Db.Connection.OpenAsync();
			var query = new BicycleDataQuery(Db);
			var result = await query.FindPageOrderByListPriceAsync(pageNumber, pageSize, ascending);
			return new OkObjectResult(result);
		}

		[HttpGet("Type/{pageNumber}/{pageSize}/{type}/{ascending}")]
		public async Task<IActionResult> GetPageOrderByTypePrice(int pageNumber, int pageSize, string type, bool ascending)
		{ 
			await Db.Connection.OpenAsync();
			var query = new BicycleDataQuery(Db);
			var result = await query.FindPageOrderByTypeAsync(pageNumber, pageSize, type, ascending);
			return new OkObjectResult(result);
		}


		[HttpGet("{id}")]
		public async Task<IActionResult> GetOne(int id)
		{
			await Db.Connection.OpenAsync();
            var query = new BicycleDataQuery(Db);
            var result = await query.FindOneAsync(id);
            if (result is null)
                return new NotFoundResult();
            return new OkObjectResult(result);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody]BicycleData body)
		{
			await Db.Connection.OpenAsync();
            body.Db = Db;
            await body.InsertAsync();
            return new OkObjectResult(body);
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> PutOne(int id, [FromBody]BicycleData body)
		{
			await Db.Connection.OpenAsync();
            var query = new BicycleDataQuery(Db);
            var result = await query.FindOneAsync(id);
            if (result is null)
                return new NotFoundResult();
			//result.Title = body.Title;
			//result.Content = body.Content;
			result.SerialNumber = body.SerialNumber;
			result.CustomerID = body.CustomerID;
			result.ModelType = body.ModelType;
			result.PaintID = body.PaintID;
			result.FrameSize = body.FrameSize;
			result.OrderDate = body.OrderDate;
			result.StartDate = body.StartDate;
			result.ShipDate = body.ShipDate;
			result.ShipEmployee = body.ShipEmployee;
			result.FrameAssembler = body.FrameAssembler;
			result.Painter = body.Painter;
			result.Construction = body.Construction;
			result.WaterBottleBrazeons = body.WaterBottleBrazeons;
			result.CustomName = body.CustomName;
			result.LetterStyleID = body.LetterStyleID;
			result.StoreID = body.StoreID;
			result.EmployeeID = body.EmployeeID;
			result.TopTube = body.TopTube;
			result.ChainStay = body.ChainStay;
			result.HeadTubeAngle = body.HeadTubeAngle;
			result.SeatTubeAngle = body.SeatTubeAngle;
			result.ListPrice = body.ListPrice;
			result.SalePrice = body.SalePrice;
			result.SalesTax = body.SalesTax;
			result.SaleState = body.SaleState;
			result.ShipPrice = body.ShipPrice;
			result.FramePrice = body.FramePrice;
			result.ComponentList = body.ComponentList;

            await result.UpdateAsync();
            return new OkObjectResult(result);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteOne(int id)
		{
			await Db.Connection.OpenAsync();
            var query = new BicycleDataQuery(Db);
            var result = await query.FindOneAsync(id);
            if (result is null)
                return new NotFoundResult();
            await result.DeleteAsync();
            return new OkResult();
		}

		[HttpDelete]
		public async Task<IActionResult> DeleteAll()
		{
            await Db.Connection.OpenAsync();
            var query = new BicycleDataQuery(Db);
            await query.DeleteAllAsync();
            return new OkResult();
		}
	}
}
